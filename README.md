# raster-img

Palettize raster based images and hide information in the image data using various steganographic algorithms.

## Installation

Build with:

    lein uberjar

Should produce output in a top-level dist directory.

## Usage

    $ java -jar raster-img-0.1.0-standalone.jar [args]

## Options

Operates in different modes:

     ["-p" "--palettize"] FILE
     ["-e" "--embed"] FILE
     ["-x" "--extract"] FILE
     ["-d" "--display"] FILE

Run the jar file without arguments to get the full arg spec.

## Examples

Palettize (`-p`) a bitmap image `foo.bmp` with a bit-depth (`--depth`) of 4 using the median cut quantization (`--quantize`) algorithm.
Perform simple dithering (`dither`).
Write the palette data to `bar.pal` and the image data to `bar.pbi`.

    $ java -jar raster-img-0.1.0-standalone.jar -p foo.bmp --depth 4 --quantize median --dither dither -o bar
    
Display the previously palettized image data:

    $ java -jar raster-img-0.1.0-standalone.jar -d bar

Embed (`-e`) secret data (`secret.txt`) in the previously palettized image data `bar`, writing the modified result to `hidden.pal` and `hidden.pbi`.
Use the Least Significant Bit (LSB) algorithm:

    $ java -jar raster-img-0.1.0-standalone.jar -e secret.txt --stego lsb -i bar -o hidden
    
View the image data with embedded information:

    $ java -jar raster-img-0.1.0-standalone.jar -d hidden

Extract (`-x`) the secret data from `hidden.pal` and `hidden.pbi`:

    $ java -jar raster-img-0.1.0-standalone.jar -x hidden --stego lsb

## License

Copyright © 2014 FIXME

Distributed under the Eclipse Public License either version 1.0 or (at
your option) any later version.
