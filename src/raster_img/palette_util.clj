(ns raster-img.palette-util
  (:require [clojure.math.numeric-tower :as math])
  (:require [raster-img.img-util :as img]))

;; Input:  Color space as vector of RGB values
;; Output: :r, :g, :b based on largest delta in
;;         each color plane
;;
;; 1. Divide the color space into r,g,b planes
;; 2. Calculate delta of each plane
;; 3. Return axis corresponding to largest delta
(defn longest-axis
  [space]
  (let [rs (map img/get-r space)
        gs (map img/get-g space)
        bs (map img/get-b space)
        dr (- (apply max rs) (apply min rs))
        dg (- (apply max gs) (apply min gs))
        db (- (apply max bs) (apply min bs))]
    (get (hash-map dr :r dg :g db :b)
         (max dr dg db))))

;; Input:  Color space as vector of RGB values,
;;         axis as :r, :g, :b
;; Output: Color space sorted according to specified axis component
(defn sort-space
  [space axis]
  (sort (fn [rgb1 rgb2]
          (compare
            (img/get-component rgb1 axis)
            (img/get-component rgb2 axis)))
        space))

;; Input:  Sorted color space as vector of RGB values,
;;         axis on which color space was sorted,
;;         pivot point in color space,
;;         predicate function comparing colors to the pivot
;; Output: Sorted color subspace according to filter predicate
;;
;; It is important to filter out the correct component along a
;; color axis because that is how the color space is ordered,
;; which affects the logic of the filter predicate.
(defn filter-space
  [space axis pivot f]
  (filter (fn [rgb]
            (f (img/get-component rgb axis)
               (img/get-component pivot axis)))
          space))

;; Input:  RGB color, palette vector
;; Output: map containing
;;         :i - integer index to palette vector
;;         :v - the palette color at index i
;;
;; Chooses a palette index based on the shortest euclidian
;; color distance to a palette color. This means that an
;; image with n pixels and palette size m will need mxn
;; comparisons to find the optimal allocation - ouch!
;;
;; Optimize - this is the bottleneck!
;; Step 1: memoization - usually the number of distinct colors
;; is a fair amount less (30%) than the total number of pixels
(def nearest
  (memoize
    (fn [rgb palette]
      (apply min-key (partial img/rgb-distance rgb)
             palette))))

;; Input:  RGB color vector, palette vector
;; Output: index vector mapping RGB values to palette entries
;;
;; Assumes a proper mapping, otherwise indices will return as -1
(defn palette-indices
  [rgb-vec palette]
  (map (fn [rgb]
         (.indexOf palette rgb))
       rgb-vec))

;; Input:  BufferedImage, index vector, palette
;; Output: BufferedImage
(defn rasterize
  [img idata palette]
  (do
    (img/set-rgb-data img (map (partial nth palette) idata))
    img))
