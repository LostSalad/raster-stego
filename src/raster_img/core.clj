(ns raster-img.core
  (:require [clojure.pprint :refer [pprint]])
  (:require [clojure.tools.cli :as cli])
  (:require [raster-img.util :as util])
  (:require [raster-img.img-util :as img])
  (:require [raster-img.palette-util :as putil])
  (:require [raster-img.assignment-util :as autil])
  (:require [raster-img.palette :as pal])
  (:require [raster-img.stego :as stego])

  (:import [mikera.gui Frames])

  (:gen-class))



(def cli-options
  [;; Operating modes
   ["-p" "--palettize"
    :required "FILE"]
   ["-e" "--embed"
    :required "FILE"]
   ["-x" "--extract"
    :required "FILE"]
   ["-d" "--display"
    :required "FILE"]

   ;; Contextual filenames
   ["-i" "--in FILE"
    "Optional input filename (eg. input.bmp) or file prefix (eg. indata) used for *.pal, *.pbi"]
   ["-o" "--out FILE"
    "Optional output filename (eg. output.bmp) or file prefix (eg. outdata) used for *.pal, *.pbi"]

   ;; Palettization options
   [nil  "--depth BITDEPTH"
    "Used to calculate palette size as 2^d. Maximum depth 8 = 256 colors"
    :parse-fn #(Integer/parseInt %)
    :validate [#(<= 1 % 8) "Must be a number between 1 and 8"]]

   [nil  "--quantize ALGORITHM"
    "Algorithm used for palette selection, specify [fixed, popularity, median]"
    :parse-fn #(case %
                 "fixed"      pal/q-fixed-palette
                 "popularity" pal/q-popularity
                 "median"     pal/q-median-cut
                 nil)
    :validate [#(not (nil? %)) "Must be one of [fixed, popularity, median]"]]

   [nil  "--dither ALGORITHM"
    "Algorithm used for dithering, specify [nearest, dither]"
    :parse-fn #(case %
                 "nearest"   pal/p-direct
                 "dither"    pal/p-dithered
                 nil)
    :validate [#(not (nil? %)) "Must be one of [nearest, dither]"]]

   ;; Embedding options
   [nil  "--stego ALGORITHM"
    "Algorithm used for embedding and extracting, specify [lsb, parity]"
    :parse-fn #(case %
                 "lsb"       {:embed   stego/embed-lsb
                              :extract stego/extract-lsb}
                 "parity"    {:embed   stego/embed-parity
                              :extract stego/extract-parity}
                 nil)
    :validate [#(not (nil? %)) "Must be one of [lsb, parity]"]]])

(defn mode-display
  [filename outfile]
  (if outfile
    (img/write-image-file (autil/read-assignment-img filename) outfile "bmp")
    (Frames/display (autil/read-assignment-img filename)
                    (str filename ".pal" " / " filename ".pbi"))))

(defn mode-palettize
  [infile outfile bit-depth q p]
  (do
    (println "Palettizing" infile ":"
             "bit-depth = " bit-depth "quantization = " q "dithering = " p)
    (println "Data will be written to"
             (str outfile ".pal") "and"
             (str outfile ".pbi"))
    (do
      (with-local-vars
        [img nil, data [], palette []]
        (do
          ;; 1) Load image file as raster image
          (println "# Loading image file as BufferedImage")
          (var-set img (img/load-image-file infile))
          (var-set data (img/get-rgb-vector-fast @img))

          ;; 2) Transform to palette-based image
          (println "# Palettizing BufferedImage")
          (var-set palette
                   (pal/palettize-img @data {:bit-depth bit-depth
                                             :quantization q
                                             :mapping p}))

          ;; 3) Store data as per assignment
          (println "# Writing assignment data")
          (autil/write-assignment-img @img @palette outfile))))))

(defn mode-embed
  [secretfile infile outfile stego-embed]
  (do
    (println "Embedding string contents of" secretfile "into image data of"
             (str infile ".pal") "and"
             (str infile ".pbi"))
    (println "New data will be written to"
             (str outfile ".pal") "and"
             (str outfile ".pbi"))
    (let [secret (str (slurp secretfile))]
      (autil/embed-stego-to-file (stego/str-to-ascii secret)
                                 infile
                                 outfile
                                 stego-embed))))

(defn mode-extract
  [infile stego-extract]
  (do
    (println "Extracting string contents embedded in"
             (str infile ".pal") "and"
             (str infile ".pbi"))
    (println "The secret message is:")
    (println (stego/ascii-to-str
               (autil/extract-stego-from-file infile stego-extract)))))

(defn mode-errors
  [parse]
  (do
    (if (:errors parse)
      (do
        (println "Invalid arguments:")
        (pprint (:errors parse))
        (println)))
    (println "Usage:")
    (println (:summary parse))))

(defn -main
  [& args]
  (let [parse (cli/parse-opts args cli-options)
        opts  (:options parse)]
    (if (nil? (:errors parse))

      ;; Run program
      (cond (:display opts)
            (mode-display (:display opts) (:out opts))

            (:palettize opts)
            (if (and (:out opts) (:depth opts) (:quantize opts) (:dither opts))
              (mode-palettize (:palettize opts) (:out opts) (:depth opts) (:quantize opts) (:dither opts))
              (println "Please specify: output file, bit depth, quantization, dither"))

            (:embed opts)
            (if (and (:in opts) (:out opts) (:stego opts))
              (mode-embed (:embed opts) (:in opts) (:out opts) (:embed (:stego opts)))
              (println "Please specify: input file, output file, stego algorithm"))

            (:extract opts)
            (if (and (:stego opts))
              (mode-extract (:extract opts) (:extract (:stego opts)))
              (println "Please specify: stego algorithm"))

            :else
            (do
              (println "Please select a mode using [-p, -e, -x, -d]")
              (mode-errors parse)))

      ;; Errors found, print arg spec
      (mode-errors parse))))
