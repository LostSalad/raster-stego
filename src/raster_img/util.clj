(ns raster-img.util)

;; Input:  collection of ordinal numbers
;; Output: list of hex-formatted strings
(defn format-hex [v]
  (map #(format "0x%x" %) v))

;; Input:  lower bound, upper bound, number
;; Output: number restricted to range [l, u]
(defn clip
  [l u x]
  (cond (<= x l) l
        (> x u) u
        :else x))
