(ns raster-img.img-util
  (:require [clojure.math.numeric-tower :as math]))

(import '(java.io File))
(import '(javax.imageio ImageIO))

;; Input:  filename
;; Output: BufferedImage or nil
(defn load-image-file
  [filename]
  (try
    (ImageIO/read (File. filename))
    (catch Exception e
      nil)))

;; Input:  BufferedImage, filename, format
;; Output: true on success, false otherwise
(defn write-image-file
  [img filename fmt]
  (try
    (ImageIO/write img fmt (File. filename))
    true
    (catch Exception e
      false)))

;; Input:  BufferedImage
;; Output: vector of RGB values in left-to-right,
;;         top-to-bottom order
;;
;; Reads the RGB values in one shot by passing a
;; standard java integer array as a buffer, and
;; converting the buffer to a clojure Vector. This
;; buffer should not be referenced further as it
;; will modify the corresponding vector.
(defn get-rgb-vector-fast
  [img]
  (let [w (.getWidth img)
        h (.getHeight img)
        s (* w h)
        b (int-array s)]
    (do
      (.getRGB img 0 0 w h b 0 w)
      (vec b))))

;; Input:  BufferedImage, RGB color vector
;; Output: BufferedImage
;;
;; Overwrites the image data with the supplied RGB values.
;; Assumes the RGB vector has enough data.
(defn set-rgb-data
  [img rgb-vector]
  (let [w (.getWidth img)
        h (.getHeight img)
        buf (int-array rgb-vector)]
    (do
      (.setRGB img 0 0 w h buf 0 w)
      img)))

;; Input:  32-bit integer representing ARGB color
;; Output: 32-bit integer representing alpha channel
(defn get-a
  [argb]
  (bit-shift-right (bit-and argb 0xFF000000) 24))

;; Input:  32-bit integer representing ARGB color
;; Output: 32-bit integer representing red channel
(defn get-r
  [argb]
  (bit-shift-right (bit-and argb 0x00FF0000) 16))

;; Input:  32-bit integer representing ARGB color
;; Output: 32-bit integer representing green channel
(defn get-g
  [argb]
  (bit-shift-right (bit-and argb 0x0000FF00) 8))

;; Input:  32-bit integer representing ARGB color
;; Output: 32-bit integer representing blue channel
(defn get-b
  [argb]
  (bit-and argb 0x000000FF))

;; Input:  32-bit integer representing ARGB color,
;;         component as :a, :r, :g, :b
;; Output: 32-bit integer representing specified channel
(defn get-component
  [argb component]
  (case component
    :a (get-a argb)
    :r (get-r argb)
    :g (get-g argb)
    :b (get-b argb)))

;; Input:  2 32-bit integers representing ARGB colors
;; Output: euclidian distance between the colors using
;;         r, g, b components
(defn rgb-distance
  [rgb1 rgb2]
  (math/sqrt (+ (math/expt (- (get-r rgb2) (get-r rgb1)) 2)
                (math/expt (- (get-g rgb2) (get-g rgb1)) 2)
                (math/expt (- (get-b rgb2) (get-b rgb1)) 2))))

;; Input:  Vector of RGB values, color component
;; Output: Vector of chosen component values only
(defn single-channel
  [rgb-vec axis]
  (map (fn [rgb] (get-component rgb axis))
       rgb-vec))

;; Input:  Vector of RGB values
;; Output: Median value that is always an element of the vector
;;
;; For an even number of elements, the median is usually calculated
;; as the mean of the middle 2 elements. The mean of the colors is
;; not desirable, so the lower of the 2 middle elements is picked.
(defn rgb-median
  [rgb-vec]
  (if (not (empty? rgb-vec))
    (let [n (count rgb-vec)
          i (quot (dec n) 2)]
      (nth rgb-vec i))
    nil))

;; Input:  RGB component
;; Output: RGB component constrained to [0, 255]
(defn clip [c]
  (cond
    (< c 0) 0
    (> c 255) 255
    :else c))

;; Input:  2 RGB values
;; Output: result of applying (op a b) piecewise to the color components
(defn rgb-op
  [op rgb1 rgb2]
  (bit-or (bit-shift-left (bit-and 0x000000FF (clip (op (get-a rgb1) (get-a rgb2)))) 24)
          (bit-shift-left (bit-and 0x000000FF (clip (op (get-r rgb1) (get-r rgb2)))) 16)
          (bit-shift-left (bit-and 0x000000FF (clip (op (get-g rgb1) (get-g rgb2)))) 8)
          (bit-shift-left (bit-and 0x000000FF (clip (op (get-b rgb1) (get-b rgb2)))) 0)))

(def rgb-add (partial rgb-op +))
(def rgb-sub (partial rgb-op -))
