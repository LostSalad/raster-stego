(ns raster-img.palette
  (:require [clojure.math.numeric-tower :as math])
  (:require [raster-img.util :as util])
  (:require [raster-img.img-util :as img])
  (:require [raster-img.palette-util :as putil]))

;; Input:  RGB vector, bit-depth, quantization function
;; Output: Palette of size 2^n where n is the bit-depth
;;
;; The bit-depth will be clipped to the range [1, 8] which
;; allows palette sizes between 2 and 256
(defn palette
  [rgb-vec bit-depth quantization-func]
  (let [size (math/expt 2 (util/clip 1 8 bit-depth))
        pal  (quantization-func rgb-vec size)
        pad  0]
    (vec (take size (flatten
                      (partition size
                                 (count pal)
                                 (repeat pad)
                                 pal))))))

;; Input:  RGB vector,
;;         bit-depth,
;;         quantization function,
;;         palette mapping function
;; Output: map
;;         :palette - palette vector of size 2^n where n is the bit-depth
;;         :idata - the indices mapping pixels to the palette
(defn palettize-img
  [rgb-vec & [{:keys [bit-depth quantization mapping]}]]
  (let [palette (palette rgb-vec bit-depth quantization)]
    {:palette palette
     :idata   (mapping rgb-vec palette)}))

;; Input:  BufferedImage, palette size
;; Output: Predefined color palette
(defn q-fixed-palette
  [rgb-vec n]
  [0xFF183F49 0xFFC14844])

;; Input:  BufferedImage, palette size
;; Output: list of n most popular RGB values by frequency
;;
;; Quantization: Popularity algorithm
(defn q-popularity
  [rgb-vec n]
  (let [freqs-asc (sort-by second (frequencies rgb-vec))
        rgbs-desc (reverse (map first freqs-asc))]
    (take n rgbs-desc)))

;; Input:  Color space as vector of RGB values, palette size
;; Output: Recursively nested vector structure with median colors
;;         at the leaves
;;
;; Recursively splits a space into subspaces using n subdivisions.
;; Base case: 1 subdivision, or no more space to divide
;;
;; Assumes elements in the space are distinct - otherwise it is easy
;; to end up with the situation where the median is the same as the
;; first or last element, which is difficult to subdivide in a way
;; that makes sense.
;;
;; 1. The longest axis of the space is chosen.
;; 2. The space is sorted along this axis
;; 3. The median color along this axis is chosen as a pivot
;; 4. The space is split into 2 subspaces:
;;      a) Colors <= the median
;;      b) Colors > the median
;;    In the base case, the median color will be returned.
;;
;; This results in a recursive vector of the general form
;;
;;                    [[m1 m2] m3]
;;
;; The overall space is divided into 2 subspaces, the first of which
;; is again subdivided.  m1, m2 and m3 are the median colors of each
;; smallest subspace.
(defn split
  [space n]
  (cond
    (or (empty? space) (< n 1))
    []

    (= 1 (count space))
    (first space)

    :else
    (let [axis         (putil/longest-axis space)
          sorted-space (putil/sort-space space axis)
          pivot        (img/rgb-median sorted-space)
          subspace-a   (putil/filter-space sorted-space axis pivot <=)
          subspace-b   (putil/filter-space sorted-space axis pivot >)]

      ;; Base case for recursion: no more subdivision
      ;; Else return a vector of 2 recursive subspaces
      (if (<= n 1)
        pivot

        [(split subspace-a (quot n 2))
         (split subspace-b (quot n 2))]))))

;; Input:  BufferedImage, palette size
;; Output: list of n RGB values from subdivided color space
;;
;; Quantization: Median Cut algorithm
;;
;; 1. Find the smallest space which contains all the colors
;;    in the image.
;; 2. Divide this space into subspaces according to median cut
;; 3. Flatten these subspaces to obtain the median color values
(defn q-median-cut
  [rgb-vec n]
  (let [color-space (distinct rgb-vec)
        split-space (split color-space n)]
    (flatten split-space)))

;; Input:  RGB color vector, palette vector
;; Output: index vector mapping RGB values to the palette
;;
;; Strategy: Nearest palette entry by euclidian distance
(defn p-direct
  [rgb-vec palette]
  (vec (map (fn [rgb]
              (.indexOf palette (putil/nearest rgb palette)))
            rgb-vec)))

;; Input:  RGB color vector, palette vector
;; Output: index vector mapping RGB values to the palette
;;
;; Strategy: Basic dithering
(defn p-dithered
  [rgb-vec palette]
  (:indices (reduce
              (fn [acc rgb]
                (let [is (:indices acc)             ; - processed indices
                      e_ (:e acc)                   ; - previous error
                      xi (img/rgb-add rgb e_)       ; - current pixel + previous error
                      yi (putil/nearest xi palette) ; - (current + error) -> palette
                      ei (img/rgb-sub rgb yi)       ; - current error (original -> final mapping)
                      i  (.indexOf palette yi)]     ; - palette index of yi
                  {:indices (conj is i) :e ei}))
              {:indices [] :e 0}                    ; - initial accumulator
              rgb-vec)))
