(ns raster-img.stego
  (:require [clojure.math.numeric-tower :as math])
  (:require [raster-img.palette-util :as putil])
  (:require [raster-img.img-util :as img]))

;; Input:  integer i,
;;         bit index n, where 1 is the least significant bit
;; Output: bit at index n
(defn bit [i n]
  (bit-and 1 (bit-shift-right i (dec n))))

;; Input:  integer i,
;;         bit count n
;; Output: seq of bits padded with 0's, ordered from most significant
;;         bit to least significant bit
(defn bits [i n]
  (map #(bit i %)
       (range n 0 -1)))

;; Input:  integer i
;; Output: least significant bit of i
(defn lsb [i] (bit-and 1 i))

;; Input:  integer i
;; Output: odd parity of i
(defn parity [i] (if (odd? i) 1 0))

;; Input:  integer i
;; Output: if i has lsb 0, i+1
;;         if i has lsb 1, i-1
(defn flip-lsb [i]
  (if (= 0 (lsb i))
    (bit-or i 1)
    (bit-and (bit-not 1) i)))

;; Input:  seq of bits, ordered from most significant bit to least significant bit,
;;         bit count n
;; Output: integer represented by n least significant bits
(defn bits-to-int [bits n]
  (reduce (fn [acc i]
            (let [rbits (reverse bits)]
              (if (< i (count rbits))
                (bit-or acc (bit-shift-left (nth rbits i) i))
                acc)))
          0
          (range 0 n)))

;; Input:  string
;; Output: seq of integers representing the ASCII values of the
;;         characters in the string
(defn str-to-ints [s]
  (map #(int (char %)) s))

;; Input:  seq of integers representing the ASCII values of the
;;         characters in the string
;; Output: string
(defn ints-to-str [is]
  (apply str (map char is)))

;; Input:  string,
;;         char size n
;; Output: seq of bits representing the ASCII values of the characters
;;         in the string. Each character takes n bits and will be
;;         padded with 0's. The string will also be null-terminated
(defn str-to-bits [s n]
  (flatten
    (map #(bits % n)
         (str-to-ints (conj (vec s) 0)))))

(defn str-to-ascii [s]
  (str-to-bits s 8))

;; Input:  seq of bits representing the ASCII values of the characters
;;         in the string, which is null-terminated,
;;         char size n
;; Output: string, where the bits are divided into chunks of size n, and
;;         converted to characters
(defn bits-to-str [bits n]
  (ints-to-str (take-while #(> % 0)
                           (map #(bits-to-int % n)
                                (partition n n (repeat 0) bits)))))

(defn ascii-to-str [bits]
  (bits-to-str bits 8))

;; Input:  image data {:pdata :idata}
;; Output: modified idata with embedded bits
(defn embed-lsb
  [adata bits]
  {:pre [(>= (count (:idata adata)) (count bits))]}

  (let [target-data (:idata adata)
        data        (vec bits)
        ndata       (count data)]
    (map-indexed (fn [i target]
                   (if (and (< i ndata)
                            (not (= (lsb target) (nth data i))))
                     (flip-lsb target)
                     target))
                 target-data)))

;; Input:  image data {:pdata :idata}
;; Output: embedded bits
(defn extract-lsb [adata]
  (map lsb (:idata adata)))

;; Input:  image data {:pdata :idata}
;; Output: modified idata with embedded bits
(defn embed-parity
  [adata bits]
  {:pre [(>= (count (:idata adata)) (count bits))]}

  (let [target-data (:idata adata)
        palette     (:palette (:pdata adata))
        data        (vec bits)
        ndata       (count data)]
    (map-indexed (fn [i target]
                   (if (< i ndata)
                     (let [rgb   (nth palette target)
                           prgb  (parity rgb)
                           pdata (parity (nth data i))]
                       (if (not (= prgb pdata))
                         (.indexOf palette
                                   (putil/nearest rgb (filter #(= pdata (parity %)) palette)))
                         target))
                     target))
                 target-data)))

;; Input:  image data {:pdata :idata}
;; Output: modified idata with embedded bits
(defn extract-parity [adata]
  (let [palette (:palette (:pdata adata))]
  (map (fn [i] (parity (nth palette i)))
       (:idata adata))))
