(ns raster-img.assignment-util
  (:require [raster-img.img-util :as img])
  (:require [raster-img.palette-util :as putil])
  (:require [raster-img.stego :as stego]))

(import '(java.awt.image BufferedImage))

;; Input:  map containing palette data (:pdata) and
;;         index data (:idata)
;; Output: filename.pal with palette header,
;;         filename.pbi with index data
(defn write-assignment-data
  [adata filename]
  (let [fpal (str filename ".pal")
        fpbi (str filename ".pbi")]
    (do
      (spit fpal (:pdata adata))
      (spit fpbi (vec (:idata adata))))))

;; Input:  BufferedImage, palette, filename prefix
;; Output: filename.pal with palette header,
;;         filename.pbi with index data
(defn write-assignment-img
  [img palette filename]
  (let [fpal (str filename ".pal")
        fpbi (str filename ".pbi")]
    (do
      (write-assignment-data {:pdata {:width   (.getWidth img)
                                      :height  (.getHeight img)
                                      :palette (:palette palette)}
                              :idata (:idata palette)}
                             filename))))

;; Input:  filename
;; Output: map containing palette data (:pdata) and
;;         index data (:idata)
(defn read-assignment-data
  [filename]
  (let [fpal  (str filename ".pal")
        fpbi  (str filename ".pbi")]
    {:pdata (read-string (slurp fpal))
     :idata (vec (read-string (slurp fpbi)))}))

;; Input:  filename prefix
;; Output: BufferedImage with rasterized palette image
(defn read-assignment-img
  [filename]
  (let [adata (read-assignment-data filename)
        pdata (:pdata adata)
        idata (:idata adata)]
    (do
      (putil/rasterize (new BufferedImage (:width pdata) (:height pdata) BufferedImage/TYPE_INT_RGB)
                       idata
                       (:palette pdata)))))

;; Input:  filename prefix
;; Output: rasterized image in display.bmp
(defn display-assignment-img
  [filename]
  (img/write-image-file (read-assignment-img filename) "display.bmp" "bmp"))

(defn embed-stego-to-file
  [bits infile outfile stego-embed]
  (let [adata (read-assignment-data infile)]
    (write-assignment-data
      (assoc adata :idata (stego-embed adata bits))
      outfile)))

(defn extract-stego-from-file
  [filename stego-extract]
  (stego-extract (read-assignment-data filename)))
