;; Non-optimal functions that should not be used but
;; are kept for reference and learning purposes
(ns experimental.img)

;; Input:  BufferedImage, index
;; Output: RGB value at (x, y) corresponding to index
;;         by scanning the image left-to-right, top-
;;         to-bottom
(defn get-linear-pixel
  [img i]
  (let [w (.getWidth img)
        h (.getHeight img)
        s (* w h)]
    (if (and (> w 0) (> h 0) (< i s) (>= i 0))
      (let [x (mod i w)
            y (quot i w)]
        (.getRGB img x y))
      nil)))

;; Input:  BufferedImage
;; Output: vector of RGB values in left-to-right,
;;         top-to-bottom order
(defn get-rgb-vector
  [img]
  (let [w (.getWidth img)
        h (.getHeight img)
        v (transient [])]
    (doall (for [x (range 0 w)
                 y (range 0 h)]
             (conj! v (.getRGB img x y))))
    (persistent! v)))

;; Input:  BufferedImage
;; Output: vector of vectors of 3x3 pixel blocks in
;;         left-to-right, top-to-bottom order
;;
;; INCOMPLETE
(defn get-3x3-block-vector
  [img]
  (let [w (.getWidth img)
        h (.getHeight img)
        v (transient [])]
    (doall (for [x (range 0 w 3)
                 y (range 0 h 3)]
             (conj! v [[(.getRGB img x y) (.getRGB img x y) (.getRGB img x y)]
                       [(.getRGB img x y) (.getRGB img x y) (.getRGB img x y)]
                       [(.getRGB img x y) (.getRGB img x y) (.getRGB img x y)]])))
    (persistent! v)))

;; Input:  BufferedImage, starting linear index
;; Output: Seq representing the linear pixels from
;;         the starting index
;;
;; Recursion and reify and linear pixels are slow :(
(defn lazy-rgb [img i]
  (let [w (.getWidth img)
        h (.getHeight img)
        n-pixels (* w h)
        data-len (dec n-pixels)]
    (seq
      (reify
        clojure.lang.ISeq
        (first [self] (get-linear-pixel img i))
        (next [self] (if (>= i data-len)
                       nil
                       (lazy-rgb img (inc i))))
        (more [self] (if (>= i data-len)
                      (seq [])
                       (lazy-rgb img (inc i))))
        (count [self] n-pixels)
        (empty [self] (or (>= i data-len) (< i 0)))
        (equiv [self obj] false)

        clojure.lang.Seqable
        (seq [self] self)))))

(defn make-lazy-rgb [img]
  (lazy-rgb img 0))

;; Input:  collection
;; Output: map of (value, freq) pairs
;;
;; Custom implementation of core frequencies function
(defn freqs [xs]
  (reduce
    (fn [freq-map n]
      (let [freq (get freq-map n)]
        (assoc freq-map n (if (nil? freq) 1 (inc freq)))))
    {} xs))
